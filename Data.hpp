#pragma once
#include "Definition.hpp"
#include "Settings.hpp"

namespace peddIM {

struct ExperimentData {
    TypeExperiment type;
    double minE;

#ifdef DEBUGMODE
    //Данные собираются только один раз на модель
    std::vector<double> alphaArray;
    std::vector<double> tArray;
    std::vector<double> eArray;
    std::vector<double> mArray;

    //Данные на каждый эксперемент
    std::vector<int64_t> startConfigSpin;
    std::vector<int64_t> minConfigSpin;
    std::vector<int64_t> field;
    int64_t fieldSum;

    int64_t countAnnealing;
#endif

    template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & type;
        ar & minE;

#ifdef DEBUGMODE
        ar & alphaArray;
        ar & tArray;
        ar & eArray;
        ar & mArray;

        ar & startConfigSpin;
        ar & minConfigSpin;
        ar & field;
        ar & fieldSum;

        ar & countAnnealing;
#endif
    }

#ifdef DEBUGMODE
    void clearDebugData() {
        alphaArray.clear();
        tArray.clear();
        eArray.clear();
        mArray.clear();
        startConfigSpin.clear();
        minConfigSpin.clear();
        field.clear();
    }
#endif
};

struct ModelData {
    int indexModel;
    std::array<std::vector<ExperimentData>, (int)TypeExperiment::Count> experiments;
};

}
