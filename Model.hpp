#pragma once
#include "PEDDForIsingModel.hpp"

namespace peddIM {
struct Model {
    //cache
    std::vector<int64_t> notSmallIndex;
    std::vector<int64_t> notBigIndex;

    //current
    std::vector<int64_t> configSpin;
    double e;
    int64_t m;
    double t;
    double alpha;
    std::vector<int64_t> field;
    int64_t fieldSum;

    //result
    double minE;
};
}
