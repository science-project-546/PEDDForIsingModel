#pragma once

#include "Definition.hpp"
#include "Model.hpp"
#include "Settings.hpp"
#include "Data.hpp"

namespace peddIM {

#ifdef DEBUGMODE
struct DebugOutputSingleton {
private:

    std::string debugFolder;
    std::array<std::string, (int)TypeExperiment::Count> startPosFileNames;
    std::array<std::string, (int)TypeExperiment::Count> minPosFileNames;
    std::array<std::string, (int)TypeExperiment::Count> fieldSampleFileNames;
    std::array<std::string, (int)TypeExperiment::Count> tPlotFileNames;
    std::array<std::string, (int)TypeExperiment::Count> alphaPlotFileNames;
    std::array<std::string, (int)TypeExperiment::Count> annealingCountNames;
    std::array<std::string, (int)TypeExperiment::Count> ePlotFileNames;

private:
    DebugOutputSingleton() {
        debugFolder = "Debug";

        for (auto i = 0; i < (int)TypeExperiment::Count; ++i) {
            auto typeString = typeExperiment2String((TypeExperiment)i);
            auto index = 0;
            startPosFileNames[i] = std::to_string(++index) + "_StartPosFile_" + typeString + ".txt";
            minPosFileNames[i] = std::to_string(++index) + "_EndPosFile_" + typeString + ".txt";

            tPlotFileNames[i] = std::to_string(++index) + "_TPlotFile_" + typeString + ".csv";
            alphaPlotFileNames[i] = std::to_string(++index) + "_AlphaPlotFile_" + typeString + ".csv";
            fieldSampleFileNames[i] = std::to_string(++index) + "_FieldSampleFile_" + typeString + ".csv";
            annealingCountNames[i] = std::to_string(++index) + "_AnnealingCount_" + typeString + ".csv";
            ePlotFileNames[i] = std::to_string(++index) + "_ePlotFile_" + typeString + ".csv";
        }

        if(fs::is_directory(debugFolder)) fs::remove_all(debugFolder);
        fs::create_directories(debugFolder);
    }
    ~DebugOutputSingleton() { }

    DebugOutputSingleton(const DebugOutputSingleton& other) = delete;
    DebugOutputSingleton& operator=(const DebugOutputSingleton& other) = delete;
    DebugOutputSingleton(DebugOutputSingleton&& other) = delete;
    DebugOutputSingleton& operator=(DebugOutputSingleton&& other) = delete;

public:

    static DebugOutputSingleton* instance() {
        static DebugOutputSingleton instant;
        return &instant;
    }

    void saveStartConfig(const ExperimentData& data, int indexModel, const Settings& settings) const {
        createOutputDirections(indexModel);
        auto currentDir = debugFolder + "/" + std::to_string(indexModel);
        std::ofstream file(currentDir + "/" + startPosFileNames[(int)data.type], std::ofstream::app);
        file << getSpinTextConfig(data.startConfigSpin, settings) << '\n';
        file.close();
    }

    void saveMinConfig(const ExperimentData& data, int indexModel, const Settings& settings) const  {
        createOutputDirections(indexModel);
        auto currentDir = debugFolder + "/" + std::to_string(indexModel);
        std::ofstream file(currentDir + "/" + minPosFileNames[(int)data.type], std::ofstream::app);
        file << getSpinTextConfig(data.minConfigSpin, settings) << '\n';
        file.close();
    }

    void saveField(const ExperimentData& data, int indexModel, int indexExperiment, const Settings &settings) const {
        createOutputDirections(indexModel);
        auto currentDir = debugFolder + "/" + std::to_string(indexModel);

        if (!fs::exists(currentDir + "/" + fieldSampleFileNames[(int)data.type])) {
            std::ofstream f(currentDir + "/" + fieldSampleFileNames[(int)data.type]);
            /*
            f << "x";
            for (auto i = 0; i < settings.sample.size(); i++) {
                f << "," << convertIndexToM(i, settings);
            }
            f << "\n";

            f << "sample";
            for (auto i : settings.sample) {
                f << "," << i;
            }
            f << "\n";
            */

            f << convertIndexToM(0, settings);
            for (auto i = 1; i < settings.sample.size(); i++) {
                f << "," << convertIndexToM(i, settings);
            }
            f << "\n";

            f << settings.sample[0];
            for (auto i = 1; i < settings.sample.size(); i++) {
                f << "," << settings.sample[i];
            }
            f << "\n";

            f.close();
        }

        std::ofstream f(currentDir + "/" + fieldSampleFileNames[(int)data.type], std::ofstream::app);
        /*
        f << "field_" + std::to_string(indexExperiment);
        for (const auto &element: data.field) {
            f << "," << element;
        }
        f << "\n";
        */

        f << data.field[0];
        for (auto i = 1; i < data.field.size(); i++) {
            f << "," << data.field[i];
        }
        f << "\n";

        f.close();
    }

    void saveTPlot(const ExperimentData& data, int indexModel, int indexExperiment, const Settings& settings) const {
        createOutputDirections(indexModel);
        auto currentDir = debugFolder + "/" + std::to_string(indexModel);

        if (!fs::exists(currentDir + "/" + tPlotFileNames[(int)data.type])) {
            std::ofstream f(currentDir + "/" + tPlotFileNames[(int)data.type]);
            //f << "x";
            f << std::to_string(0);
            for (auto i = settings.statisticsStep; i <= settings.countIteration; i += settings.statisticsStep) {
                f << "," << std::to_string(i);
            }
            f << "\n";
            f.close();
        }

        std::ofstream f(currentDir + "/" + tPlotFileNames[(int)data.type], std::ofstream::app);
        //f << "e_" + std::to_string(indexExperiment);
        f << data.tArray[0];
        for (auto i = 1; i < data.tArray.size(); i++) {
            f << "," << data.tArray[i];
        }
        f << "\n";
        f.close();
    }

    void saveAlphaPlot(const ExperimentData& data, int indexModel, int indexExperiment, const Settings& settings) const {
        createOutputDirections(indexModel);
        auto currentDir = debugFolder + "/" + std::to_string(indexModel);

        if (!fs::exists(currentDir + "/" + alphaPlotFileNames[(int)data.type])) {
            std::ofstream f(currentDir + "/" + alphaPlotFileNames[(int)data.type]);
            //f << "x";
            f << std::to_string(0);
            for (auto i = settings.statisticsStep; i <= settings.countIteration; i += settings.statisticsStep) {
                f << "," << std::to_string(i);
            }
            f << "\n";
            f.close();
        }

        std::ofstream f(currentDir + "/" + alphaPlotFileNames[(int)data.type], std::ofstream::app);
        //f << "e_" + std::to_string(indexExperiment);
        f << data.alphaArray[0];
        for (auto i = 1; i < data.alphaArray.size(); i++) {
            f << "," << data.alphaArray[i];
        }
        f << "\n";
        f.close();
    }

    void saveEPlot(const ExperimentData& data, int indexModel, int indexExperiment, const Settings& settings) const {
        createOutputDirections(indexModel);
        auto currentDir = debugFolder + "/" + std::to_string(indexModel);

        if (!fs::exists(currentDir + "/" + ePlotFileNames[(int)data.type])) {
            std::ofstream f(currentDir + "/" + ePlotFileNames[(int)data.type]);
            //f << "x";
            f << std::to_string(0);
            for (auto i = settings.statisticsStep; i <= settings.countIteration; i += settings.statisticsStep) {
                f << "," << std::to_string(i);
            }
            f << "\n";
            f.close();
        }

        std::ofstream f(currentDir + "/" + ePlotFileNames[(int)data.type], std::ofstream::app);
        //f << "e_" + std::to_string(indexExperiment);
        f << data.eArray[0];
        for (auto i = 1; i < data.eArray.size(); i++) {
            f << "," << data.eArray[i];
        }
        f << "\n";
        f.close();
    }

    void saveAnnealingCount(const ExperimentData& data, int indexModel, int indexExperiment) const {
        createOutputDirections(indexModel);
        auto currentDir = debugFolder + "/" + std::to_string(indexModel);

        /*
        if (!fs::exists(currentDir + "/" + annealingCountNames[(int)data.type])) {
            std::ofstream f(currentDir + "/" + annealingCountNames[(int)data.type]);
            f << "Experiment,AnnealingCount\n";
            f.close();
        }
        */

        std::ofstream f(currentDir + "/" + annealingCountNames[(int)data.type], std::ofstream::app);
        f << indexExperiment << "," << data.countAnnealing  << "\n";
        f.close();
    }

#if TRANSPOSEFIELDMATRIX
    void transposeFieldFile(int indexModel) const {
        auto currentDir = debugFolder + "/" + std::to_string(indexModel);

        for (auto i = 0; i < static_cast<int>(TypeExperiment::Count); ++i) {
            const auto type = typeExperiment2String(static_cast<TypeExperiment>(i));
            {
                const auto file = currentDir + "/" + fieldSampleFileNames[i];
                if(fs::exists(file)) {
                    const auto fileTmp = currentDir + "/" + fieldSampleFileNames[i] + "_tmp";
                    system(("~/.opam/default/bin/csvtool transpose " + file + " > " + fileTmp).c_str());
                    boost::filesystem::remove(file);
                    boost::filesystem::rename(fileTmp, file);
                }
            }
            {
                const auto file = currentDir + "/" + tPlotFileNames[i];
                if(fs::exists(file)) {
                    const auto fileTmp = currentDir + "/" + tPlotFileNames[i] + "_tmp";
                    system(("~/.opam/default/bin/csvtool transpose " + file + " > " + fileTmp).c_str());
                    boost::filesystem::remove(file);
                    boost::filesystem::rename(fileTmp, file);
                }
            }
            {
                const auto file = currentDir + "/" + alphaPlotFileNames[i];
                if(fs::exists(file)) {
                    const auto fileTmp = currentDir + "/" + alphaPlotFileNames[i] + "_tmp";
                    system(("~/.opam/default/bin/csvtool transpose " + file + " > " + fileTmp).c_str());
                    boost::filesystem::remove(file);
                    boost::filesystem::rename(fileTmp, file);
                }
            }
            {
                const auto file = currentDir + "/" + ePlotFileNames[i];
                if(fs::exists(file)) {
                    const auto fileTmp = currentDir + "/" + ePlotFileNames[i] + "_tmp";
                    system(("~/.opam/default/bin/csvtool transpose " + file + " > " + fileTmp).c_str());
                    boost::filesystem::remove(file);
                    boost::filesystem::rename(fileTmp, file);
                }
            }
        }
    }
#endif
private:

    BOOST_FORCEINLINE static std::string getSpinTextConfig(const std::vector<int64_t> &configSpin, const Settings &settings) {
        std::string result = "(";

        for (const auto &val: configSpin) {
            std::stringstream stream;
            stream << std::fixed << std::setprecision(1) << settings.indexToLength[val];
            result += stream.str() + ";";
        }
        result += ")";
        return result;

    }

    BOOST_FORCEINLINE void createOutputDirections(int indexModel) const {
        auto currentDir = debugFolder + "/" + std::to_string(indexModel);
        if(!fs::is_directory(currentDir)) fs::create_directories(currentDir);
    }
};
#endif

struct OutputSingleton {
private:
    std::string resultFolder;
    std::string eName;
    std::string eDeltaName;

private:
    OutputSingleton() {
        resultFolder = "Result";
        eName = "eFile.csv";
        eDeltaName = "eDeltaFile.csv";

        if(fs::is_directory(resultFolder)) fs::remove_all(resultFolder);
        fs::create_directories(resultFolder);
    }
    ~OutputSingleton() { }

    OutputSingleton(const OutputSingleton& other) = delete;
    OutputSingleton& operator=(const OutputSingleton& other) = delete;
    OutputSingleton(OutputSingleton&& other) = delete;
    OutputSingleton& operator=(OutputSingleton&& other) = delete;

public:

    static OutputSingleton* instance() {
        static OutputSingleton instant;
        return &instant;
    }

    void saveModelResult(const ModelData& data) {        
        {
            auto filePath = resultFolder + "/" + eName;
            if (!fs::exists(filePath)) {
                std::ofstream f(filePath);
                f << "x";
                for(auto i = 0; i < (int)TypeExperiment::Count; i++) {
                    const auto type = typeExperiment2String(static_cast<TypeExperiment>(i));
                    f << "," << "minE_" << type;
                    f << "," << "<minE>_" << type;
                }
                f << "\n";
                f.close();
            }
            std::ofstream f(filePath, std::ofstream::app);

            f << data.indexModel;
            for(auto i = 0; i < (int)TypeExperiment::Count; i++) {
                std::vector<double> eArray;
                for(auto& element : data.experiments[i])
                    eArray.push_back(element.minE);

                auto minE = (*std::min_element(eArray.cbegin(), eArray.cend())) * 1.f;
                auto meanMinE = std::accumulate(eArray.cbegin(), eArray.cend(), 0.f)
                        / (float)eArray.size();

                f << "," << minE;
                f << "," << meanMinE;
            }
            f << "\n";
            f.close();
        }
        {
            auto filePath = resultFolder + "/" + eDeltaName;
            std::ofstream f(filePath, std::ofstream::app);


            std::vector<double> eArray;
            for(auto& element : data.experiments[(int)TypeExperiment::Annealing])
                eArray.push_back(element.minE);

            auto minEAnnealing = (*std::min_element(eArray.cbegin(), eArray.cend())) * 1.f;
            auto meanMinEAnnealing = std::accumulate(eArray.cbegin(), eArray.cend(), 0.f)
                    / (float)eArray.size();

            f << data.indexModel;
            for(auto i = 0; i < (int)TypeExperiment::Count; i++) {
                if(i == (int)TypeExperiment::Annealing) continue;

                std::vector<double> eArray;
                for(auto& element : data.experiments[i])
                    eArray.push_back(element.minE);

                auto minE = (*std::min_element(eArray.cbegin(), eArray.cend())) * 1.f;
                auto meanMinE = std::accumulate(eArray.cbegin(), eArray.cend(), 0.f)
                        / (double)eArray.size();

                f << "," << minE - minEAnnealing;
                f << "," << meanMinE - meanMinEAnnealing;
            }
            f << "\n";
            f.close();
        }
    }
};

};
