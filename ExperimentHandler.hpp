#pragma once
#include "Definition.hpp"
#include "Settings.hpp"
#include "Model.hpp"
#include "FreeFunctions.hpp"
#include "Data.hpp"

namespace peddIM {
struct ExperimentHandler {
private:
    RNGType generator;
    boost::uniform_01<> getRandom01;

    Settings settings;
    ModelSettings modelSettings;

public:

    explicit ExperimentHandler(const Settings &settings)
        : settings(settings) {
        boost::random_device rd("/dev/urandom");
        generator = RNGType(rd());
    }

    void setModelSettings(const ModelSettings& modelSettings) {
        this->modelSettings = modelSettings;
    }

    ExperimentData execute(TypeExperiment type) {
        mpi::communicator world;

        auto data = ExperimentData();
        auto model = getModel(data);

        if(type == TypeExperiment::Annealing) {
            executeAnnealing(model, data);
        } else if (type == TypeExperiment::AnnealingPEDD) {
            executeAnnealingPEDD(model, data);
        } else {
            std::cout << "TypeExperiment not found" << std::endl;
            world.abort(-1);
        }

        data.type = type;
        data.minE = model.minE;

#ifdef DEBUGMODE
        data.field = model.field;
        data.fieldSum = model.fieldSum;
#endif

        return data;
    }

    double getMeanT() {
        auto data = ExperimentData();
        auto model = getModel(data);
        boost::uniform_int<> getRandomSpin(0, settings.sizeModel - 1);

        double sumDelta = 0;
        auto count = 10000;

        for(auto i = 0; i < count; i++) {
            auto index = getRandomSpin(generator);

            int direction;
            if (model.configSpin[index] + 1 == settings.indexToLength.size())
                direction = -1;
            else if (model.configSpin[index] - 1 < 0)
                direction = 1;
            else {
                auto beta = getRandom01(generator);
                if (beta > 0.5)
                    direction = 1;
                else
                    direction = -1;
            }

            auto oldLocalE = getLocalE(model.configSpin, settings, modelSettings, index);

            model.configSpin[index] += direction;
            auto newLocalE = getLocalE(model.configSpin, settings, modelSettings, index);

            auto delta = newLocalE - oldLocalE;
            if(delta > 0)
                sumDelta += delta;
        }

        return sumDelta / count;
    }

private:

    void executeAnnealing(Model& model, ExperimentData& data) {

#ifdef DEBUGMODE
        int countStatistic = 0;
        data.tArray.push_back(model.t);
        data.alphaArray.push_back(model.alpha);
        data.eArray.push_back(model.e);
#endif
        int countT = 0;

        bool isPlateau = false;
        auto countPlateau = settings.countPlateau;
        double previousE = model.e;

        for (auto i = 0; i < settings.countIteration; ++i) {
            mk(model);

            if (model.e < model.minE) {
                model.minE = model.e;
#ifdef DEBUGMODE
                data.minConfigSpin = model.configSpin;
#endif
            }

            //Понижаем температуру
            countT++;
            if (countT == settings.dropTStep) {
                model.t -= settings.factorT * model.t;
                countT = 0;
            }

#ifdef DEBUGMODE
            countStatistic++;
            if (countStatistic == settings.statisticsStep) {
                data.tArray.push_back(model.t);
                data.alphaArray.push_back(model.alpha);
                data.eArray.push_back(model.e);
                countStatistic = 0;
            }
#endif

            if(isPlateau) {
                countPlateau--;
                if(countPlateau < 0) {
                    isPlateau = false;
                    model.t = 2 * modelSettings.meanT;
                    countPlateau = settings.countPlateau;
#ifdef DEBUGMODE
                    data.countAnnealing++;
#endif
                }
            }
            else  {
                auto delta = model.e - previousE;
                if(delta > 0) {
                    auto w = std::exp(-delta / (settings.kBoltzmann * model.t));
                    if(w < settings.smallW) {
                        isPlateau = true;
                    }
                }
            }
            previousE = model.e;
        }
    }

    void executeAnnealingPEDD(Model& model, ExperimentData& data) {

#ifdef DEBUGMODE
        int countStatistic = 0;
        data.tArray.push_back(model.t);
        data.alphaArray.push_back(model.alpha);
        data.eArray.push_back(model.e);
#endif

        int timeMK = 0;
        int timePedd = 0;
        bool isMK = true;
        {
            model.t = -modelSettings.meanT / std::log(settings.smallWAnnealingPedd);
            model.t = std::fabs(model.t);
        }

        for (auto i = 0; i < settings.countIteration; ++i) {
            if (isMK) {
                mk(model);
                timeMK++;
            } else {
                pedd(model);
                timePedd++;
            }

            if (model.e < model.minE) {
                model.minE = model.e;
#ifdef DEBUGMODE
                data.minConfigSpin = model.configSpin;
#endif
            }

#ifdef DEBUGMODE
            countStatistic++;
            if (countStatistic == settings.statisticsStep) {
                data.tArray.push_back(model.t);
                data.alphaArray.push_back(model.alpha);
                data.eArray.push_back(model.e);
                countStatistic = 0;
            }
#endif
            if(timeMK >= settings.timeMK || timePedd >= settings.timePedd)
            {
                timeMK = 0;
                timePedd = 0;

#ifdef DEBUGMODE
                if(isMK) data.countAnnealing++;
#endif
                isMK = !isMK;
            }
        }
    }

    void pedd(Model &model) {
        auto leftRightShift = getLeftRightM(model);

        double currentH = std::numeric_limits<double>::infinity();
        auto shiftIndex = 0;

        for (int i = 0; i < leftRightShift.size(); i++) {
            auto shift = leftRightShift[i];
            model.configSpin[shift.first] += shift.second;

            auto newM = model.m + shift.second;
            auto fieldIndex = convertMToIndex(newM, settings);

            auto h1 = static_cast<double>(settings.sample[fieldIndex]) -
                    static_cast<double>(settings.sampleSum) / static_cast<double>(model.fieldSum + 1) *
                    static_cast<double>(model.field[fieldIndex] + 1);
            if (h1 < 0) h1 = 0;

            auto h2 = static_cast<double>(settings.sample[fieldIndex]) -
                    static_cast<double>(settings.sampleSum) / static_cast<double>(model.fieldSum) *
                    static_cast<double>(model.field[fieldIndex]);
            if (h2 < 0) h2 = 0;

            auto h = h1 - h2;

            if (h < currentH) {
                currentH = h;
                shiftIndex = i;
            }
            model.configSpin[shift.first] -= shift.second;
        }
        auto shift = leftRightShift[shiftIndex];

        auto oldLocalE = getLocalE(model.configSpin, settings, modelSettings, shift.first);
        model.configSpin[shift.first] += shift.second;
        auto newLocalE = getLocalE(model.configSpin, settings, modelSettings, shift.first);
        auto delta = newLocalE - oldLocalE;

        model.m += shift.second;
        model.e += delta;

        auto fieldIndex = convertMToIndex(model.m, settings);
        model.field[fieldIndex] += 1;
        model.fieldSum += 1;

        checkLeftRightArray(model, shift.first);
    }

    void mk(Model &model) {
        mpi::communicator world;
        boost::uniform_int<> getRandomSpin(0, settings.sizeModel - 1);
        auto index = getRandomSpin(generator);

        int direction;
        if (model.configSpin[index] + 1 == settings.indexToLength.size())
            direction = -1;
        else if (model.configSpin[index] - 1 < 0)
            direction = 1;
        else {
            auto beta = getRandom01(generator);
            if (beta > 0.5)
                direction = 1;
            else
                direction = -1;
        }

        auto oldLocalE = getLocalE(model.configSpin, settings, modelSettings, index);
        model.configSpin[index] += direction;

        auto newLocalE = getLocalE(model.configSpin, settings, modelSettings, index);
        model.configSpin[index] -= direction;

        auto delta = newLocalE - oldLocalE;

        if (delta < 0) {
            model.configSpin[index] += direction;
            model.e += delta;
            model.m += direction;
            checkLeftRightArray(model, index);
        } else {
            auto w = std::exp(-delta / (settings.kBoltzmann * model.t));
            auto beta = getRandom01(generator);
            if (beta <= w) {
                model.configSpin[index] += direction;
                model.e += delta;
                model.m += direction;
                checkLeftRightArray(model, index);
            }
        }

        auto fieldIndex = convertMToIndex(model.m, settings);
        model.field[fieldIndex] += 1;
        model.fieldSum += 1;
    }

    BOOST_FORCEINLINE std::vector<std::pair<int64_t, int64_t>> getLeftRightM(const Model &model) {
        std::vector<std::pair<int64_t, int64_t>> result;
        auto indexToLengthSize = static_cast<int64_t>(settings.indexToLength.size());
        if (!model.notSmallIndex.empty()) {
            boost::uniform_int<> getIndex(0, static_cast<int>(model.notSmallIndex.size()) - 1);
            auto i = getIndex(generator);
            auto val = model.notSmallIndex[i];
            result.emplace_back(val, -1);
            if (model.configSpin[val] < indexToLengthSize - 1) result.emplace_back(val, 1);
        }
        if (!model.notBigIndex.empty()) {
            boost::uniform_int<> getIndex(0, static_cast<int>(model.notBigIndex.size()) - 1);
            auto i = getIndex(generator);
            auto val = model.notBigIndex[i];
            result.emplace_back(val, 1);
            if (model.configSpin[val] > 0) result.emplace_back(val, -1);
        }
        std::shuffle(result.begin(), result.end(), generator);
        return result;
    }

    BOOST_FORCEINLINE void checkLeftRightArray(Model &model, int64_t index) {
        auto indexToLengthSize = static_cast<int>(settings.indexToLength.size());

        //std::cout << "checkLeftRightArray" << std::endl;
        {
            auto it = std::find(model.notSmallIndex.begin(), model.notSmallIndex.end(), index);
            if (it == model.notSmallIndex.end() && model.configSpin[index] > 0) {
                model.notSmallIndex.push_back(index);
            }
            if (it != model.notSmallIndex.end() && model.configSpin[index] == 0) {
                model.notSmallIndex.erase(it);
            }
        }
        {
            auto it = std::find(model.notBigIndex.begin(), model.notBigIndex.end(), index);
            if (it != model.notBigIndex.end() && model.configSpin[index] == indexToLengthSize - 1) {
                model.notBigIndex.erase(it);
            }
            if (it == model.notBigIndex.end() && model.configSpin[index] < indexToLengthSize - 1) {
                model.notBigIndex.push_back(index);
            }
        }
    }

    Model getModel(ExperimentData& data) {
        Model model;
        boost::uniform_int<> getRandomLength(0, static_cast<int>(settings.indexToLength.size() - 1));

        auto indexToLengthSize = static_cast<int>(settings.indexToLength.size());
        for (int i = 0; i < settings.sizeModel; i++) {
            model.configSpin.push_back(getRandomLength(generator));
        }
#ifdef DEBUGMODE
        data.startConfigSpin = model.configSpin;
        data.minConfigSpin = model.configSpin;
#endif

        model.e = getE(model.configSpin, settings, modelSettings);
        model.m = getM(model.configSpin, settings);
        model.t = settings.startT;
        model.alpha = settings.startAlpha;

        model.field.resize(settings.sample.size(), 0);
        auto fieldIndex = convertMToIndex(model.m, settings);
        model.field[fieldIndex] += 1;
        model.fieldSum = 1;

        model.minE = model.e;
        //std::cout << model.minE << "\n";

        model.notBigIndex.reserve(settings.sizeModel);
        model.notSmallIndex.reserve(settings.sizeModel);
        for (int i = 0; i < settings.sizeModel; i++) {
            auto val = model.configSpin[i];
            if (val > 0) model.notSmallIndex.push_back(i);
            if (val < indexToLengthSize - 1) model.notBigIndex.push_back(i);
        }

        return model;
    }

};

}
