#pragma once
#include "Definition.hpp"
#include "Settings.hpp"

namespace peddIM {

BOOST_FORCEINLINE static double getE(const std::vector<int64_t> &configSpin, const Settings &settings, const ModelSettings &modelSettings) {
    std::vector<double> sumE(modelSettings.interactions.size(), 0);
    double sumM = 0;
    for (int64_t i = 0; i < configSpin.size(); i++) {
        for (int64_t j = 0; j < sumE.size(); j++) {
            int64_t rightIndex = i + j + 1;
            //if (rightIndex >= configSpin.size()) rightIndex -= settings.size;
            if (rightIndex >= configSpin.size()) continue;
            sumE[j] += settings.indexToLength[configSpin[i]] * settings.indexToLength[configSpin[rightIndex]];
        }
        sumM += settings.indexToLength[configSpin[i]];
    }

    double energy = 0;
    for (auto i = 0; i < sumE.size(); i++) {
        energy -= modelSettings.interactions[i] * sumE[i];
    }
    energy -= modelSettings.h * sumM;
    return energy;
}

BOOST_FORCEINLINE static double getLocalE(const std::vector<int64_t> &configSpin, const Settings &settings, const ModelSettings &modelSettings, int64_t spinIndex) {
    const auto &indexToLength = settings.indexToLength;
    const auto &interactionArray = modelSettings.interactions;

    std::vector<int64_t> localConfigSpin;
    int64_t minIndex = 0;
    if (spinIndex > modelSettings.interactions.size()) minIndex = spinIndex - static_cast<int64_t>(modelSettings.interactions.size());

    int64_t maxIndex = spinIndex + static_cast<int64_t>(modelSettings.interactions.size()) + 1;
    if (maxIndex >= configSpin.size()) maxIndex = static_cast<int64_t>(configSpin.size());
    for (auto i = minIndex; i < maxIndex; ++i) {
        localConfigSpin.push_back(configSpin[i]);
    }
    return getE(localConfigSpin, settings, modelSettings);
}

BOOST_FORCEINLINE static int64_t getM(const std::vector<int64_t> &configSpin, const Settings &settings) {
    double m = 0;
    for (auto i: configSpin) {
        m += settings.indexToLength[i];
    }
    return static_cast<int64_t>(m);
}

BOOST_FORCEINLINE static int64_t convertIndexToM(int64_t i, const Settings &settings) {
    auto maxLengthSpin = std::fabs(settings.indexToLength[0]);
    return static_cast<int64_t>(i - maxLengthSpin * settings.sizeModel);
}

BOOST_FORCEINLINE static int64_t convertMToIndex(int64_t m, const Settings &settings) {
    auto maxLengthSpin = std::fabs(settings.indexToLength[0]);
    return static_cast<int64_t>(m + maxLengthSpin * settings.sizeModel);
}

}
