#pragma once
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/timer/timer.hpp>
#include <boost/filesystem.hpp>
#include <boost/date_time.hpp>
#include <boost/chrono/duration.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/range/irange.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/nondet_random.hpp>
#include <string>
#include <vector>
#include <array>
#include <cstdlib>
#include <boost/mpi.hpp>
#include <numeric>
#include "libs/rapidcsv.h"

namespace peddIM {
typedef boost::mt19937 RNGType;
namespace mp = boost::multiprecision;
namespace mpi = boost::mpi;
namespace timer = boost::timer;
namespace fs = boost::filesystem;
}
