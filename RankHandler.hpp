#pragma once
#include "PEDDForIsingModel.hpp"
#include "Settings.hpp"
#include "ExperimentHandler.hpp"
#include "OutputSingleton.hpp"
#include "InputHandler.hpp"

namespace peddIM {

enum class RankCommand {
    NewSettings,
    NewModelSettings,
    NewExperiment,
    End,
};

struct MainRankHandler {
private:
    std::unique_ptr<ExperimentHandler> experimentHandler;

public:

    void execute() {
        mpi::communicator world;

        auto inputHandler = InputHandler::instance();
        auto settings = inputHandler->getSettings();
        experimentHandler = std::unique_ptr<ExperimentHandler>(new ExperimentHandler(settings));

        std::vector<ModelSettings> modelSettings;
        inputHandler->getModelSettings(modelSettings);

        {
            auto command = RankCommand::NewSettings;
            broadcast(world, command, 0);
            broadcast(world, settings, 0);
        }

        std::vector<ExperimentData> experimentArray;

        auto outputSingleton = OutputSingleton::instance();

        for(auto modelIndex = 0; modelIndex < modelSettings.size(); modelIndex++) {
            std::cout << "Start model " << modelIndex << "!" << std::endl;
            ModelData modelData;
            modelData.indexModel = modelIndex;

            {
                auto command = RankCommand::NewModelSettings;
                broadcast(world, command, 0);
                broadcast(world, modelSettings[modelIndex], 0);
                experimentHandler->setModelSettings(modelSettings[modelIndex]);
            }

            for(auto experimentIndex = 0; experimentIndex < settings.countExperiment;) {
                for(auto typeIndex = 0; typeIndex < (int)TypeExperiment::Count; typeIndex++) {
                    experimentArray.clear();
                    auto command = RankCommand::NewExperiment;
                    auto typeExperiment = (TypeExperiment)typeIndex;
                    broadcast(world, command, 0);
                    broadcast(world, typeExperiment, 0);

                    auto experimentData = experimentHandler->execute(typeExperiment);
                    gather(world, experimentData, experimentArray, 0);

#ifdef DEBUGMODE
                    {
                        auto currentExperimentIndex = experimentIndex;
                        auto debugOutput = DebugOutputSingleton::instance();
                        for(auto i = 0; i < experimentArray.size(); i++) {
                            debugOutput->saveTPlot(experimentArray[i], modelIndex, currentExperimentIndex, settings);
                            debugOutput->saveEPlot(experimentArray[i], modelIndex, currentExperimentIndex, settings);
                            debugOutput->saveStartConfig(experimentArray[i], modelIndex, settings);
                            debugOutput->saveMinConfig(experimentArray[i], modelIndex, settings);
                            debugOutput->saveAnnealingCount(experimentArray[i], modelIndex, currentExperimentIndex);
                            if((TypeExperiment)typeIndex == TypeExperiment::AnnealingPEDD) {
                                debugOutput->saveAlphaPlot(experimentArray[i], modelIndex, currentExperimentIndex, settings);
                                debugOutput->saveField(experimentArray[i], modelIndex, currentExperimentIndex, settings);
                            }

                            experimentArray[i].clearDebugData();

                            currentExperimentIndex++;
                            if(currentExperimentIndex >= settings.countExperiment) break;
                        }
                    }
#endif
                    auto currentExperimentIndex = experimentIndex;
                    for(auto i = 0; i < experimentArray.size(); i++) {
                        modelData.experiments[typeIndex].push_back(experimentArray[i]);
                        currentExperimentIndex++;
                        if(currentExperimentIndex >= settings.countExperiment) break;
                    }
                }
                experimentIndex += world.size();
            }

            outputSingleton->saveModelResult(modelData);

#ifdef TRANSPOSEFIELDMATRIX
#ifdef DEBUGMODE
            auto debugOutput = DebugOutputSingleton::instance();
            debugOutput->transposeFieldFile(modelIndex);
#endif
#endif

        }

        {
            auto command = RankCommand::End;
            broadcast(world, command, 0);
        }
    }
};

struct SupportRankHandler {
private:
    std::unique_ptr<ExperimentHandler> experimentHandler;

public:
    void execute() {
        mpi::communicator world;

        RankCommand command;
        while (true) {
            broadcast(world, command, 0);

            if(command == RankCommand::NewSettings) {
                Settings settings;
                broadcast(world, settings, 0);
                experimentHandler = std::unique_ptr<ExperimentHandler>(new ExperimentHandler(settings));

            } else if (command == RankCommand::NewModelSettings) {
                ModelSettings modelSettings;
                broadcast(world, modelSettings, 0);
                experimentHandler->setModelSettings(modelSettings);

            } else if (command == RankCommand::NewExperiment) {
                TypeExperiment type;
                broadcast(world, type, 0);
                auto experimentData = experimentHandler->execute(type);
                gather(world, experimentData, 0);

            } else if (command == RankCommand::End) {
                break;
            }
        }
    }
};

}
