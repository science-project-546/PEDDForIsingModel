import os
import random
import time
import matplotlib.pyplot as plt
import numpy as np
import math

startTime = time.time()

path = os.getcwd() + "/output"

if not os.path.isdir(path):
        os.mkdir(path)

n = int(1e3)
m = int(1e4)
array = np.zeros(int(2 * m + 1))
for i in np.arange(0, n, 1):
    p_cur = int(array.size / 2)
    for j in np.arange(0, m, 1):
        p_cur += 1
        beta = random.random()
        if beta < 0.5:
            p_cur -= 2
        if p_cur < 0:
             p_cur = 0
        if p_cur == array.size:
             p_cur = array.size - 1

        array[p_cur] += 1

array /= n;

plt.figure(figsize=(8, 8));
font_size = 16
plt.xlabel('X', size=font_size)
plt.ylabel('N', size=font_size)
plt.xticks(size=font_size)
plt.yticks(size=font_size)
delta = 400
plt.plot(np.arange(-delta, delta, 1), array[int(array.size / 2) - delta:int(array.size / 2) + delta])
plt.savefig(path +"/exp_6.png")

# Времени потрачено
time_spent = time.time() - startTime
print("timeSpent: " + str(time_spent))
