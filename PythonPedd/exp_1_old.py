import collections
import csv
import os
import random
import shutil
import time
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
import math
from multiprocessing import Manager, Process, Queue

path = os.getcwd() + "/output"

a = 1
b = 6

x_shift = 0.5
x_delta = 1
y_delta = 0.5 * math.sqrt(3)


def h(point: np.array, point_p: np.array):
    return -a * math.exp(-b*((point[0] - point_p[0])**2 + (point[1]-point_p[1])**2))


def main_function(point: np.array, array: np.array):
    result = 0
    for point_p in array:
        result += h(point, point_p)

    return result


def create_field(x_size, y_size):
    result = []

    for y in range(y_size):
        for x in range(x_size):
            point_x = x * x_delta
            if y % 2 != 0:
                point_x += x_shift
            point_y = y * y_delta
            result.append(np.array([point_x, point_y]))

    return np.array(result)


def image_function():
    x_size = 3
    y_size = 3
    field = create_field(x_size, y_size)
    # print(field)

    result = []

    for y in np.arange(-1, y_size - 0.5, 0.01):
        tmp = []
        for x in np.arange(-1, x_size + x_shift, 0.01):
            tmp.append(main_function(np.array([x, y]), field))
        result.append(tmp)

    result = np.matrix(result)
    # print(result)
    img = plt.contour(result, extent=[-1, x_size + x_shift, y_size - 0.5, -1])
    plt.savefig("image_function.png")

    return


def mk(point: np.array, field: np.array, t):
    new_point = np.copy(point)
    phi = random.uniform(0, 2 * math.pi)
    r = 0.01
    new_point += np.array([r * math.cos(phi), r * math.sin(phi)])
    delta = main_function(new_point, field) - main_function(point, field)

    if delta < 0:
        return new_point

    w = math.exp(-delta / (1 * t))
    beta = random.random()
    if beta <= w:
        return new_point

    return point


def experiment(field, start_point, n, t, result):
    r = 0
    point = np.copy(start_point)
    f = open(path + "/t_" + str(t) + ".csv", "w")
    f.write("x,y,r\n")
    f.write(str(point[0]) + "," + str(point[1]) + "," + str(0) + "\n")

    for i in range(n):
        point = mk(point, field, t)
        new_r = np.linalg.norm(point-start_point)
        f.write(str(point[0]) + "," + str(point[1]) + "," + str(new_r) + "\n")
        r += new_r

    result[t] = r / n
    f.close()
    return


def main():
    startTime = time.time()
    if os.path.isdir(path):
        shutil.rmtree(path)
    os.mkdir(path)

    t_step = 0.01
    t_min = 0.01
    t_max = 5
    x_size = 12
    y_size = 12
    n = 100000
    field = create_field(x_size, y_size)
    start_point = np.array([(x_size + x_shift) / 2, y_size * y_delta / 2])

    manager = Manager()
    result = manager.dict()

    jobs = []
    for t in np.arange(t_min, t_max, t_step):
        p = Process(target=experiment, args=(
            np.copy(field), np.copy(start_point), n, t, result))
        jobs.append(p)

    job_length = len(jobs)
    for i, job in enumerate(jobs):
        job.start()
        if i % 16 == 0 and i != 0:
            job.join()
            print("Finish: " + str(i) + "/" + str(job_length))

    for job in jobs:
        job.join()

    # Времени потрачено
    time_spent = time.time() - startTime
    print("timeSpent: " + str(time_spent))

    t_result = []  # np.array(result.keys())
    r_result = []  # np.array(result.values())
    od = collections.OrderedDict(sorted(result.items()))
    for t, r in od.items():
        t_result.append(t)
        r_result.append(r)
    plt.plot(t_result, r_result)
    plt.savefig("exp.png")

    return


def point_plot():
    list = ['output/t_0.01.csv', 'output/t_1.0.csv', 'output/t_2.0.csv']
    x_list = []
    y_list = []

    for file_path in list:
        with open(file_path, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            x = []
            y = []
            for row in reader:
                x.append(float(row['x']))
                y.append(float(row['y']))

            x_list.append(np.array(x))
            y_list.append(np.array(y))

    plt.plot(x_list[2], y_list[2])
    plt.plot(x_list[1], y_list[1])
    plt.plot(x_list[0], y_list[0])
    plt.legend(['t=2.0', 't=1.0', 't=0.01'], loc=2)
    plt.savefig("point_plot.png")
    return


def image_rad_and_peaks():
    x_size = 12
    y_size = 12

    field = create_field(x_size, y_size)
    # print(field)

    peak_plot = []

    for y in np.arange(-1, y_size - 0.5, 0.01):
        tmp = []
        for x in np.arange(-1, x_size + x_shift, 0.1):
            tmp.append(main_function(np.array([x, y]), field))
        peak_plot.append(tmp)

    peak_plot = np.matrix(peak_plot)
    # print(result)

    list = [
        'output/t_0.01.csv',
        'output/t_1.0.csv',
        'output/t_2.0.csv',
        'output/t_3.0.csv',
        'output/t_3.88.csv',
        'output/t_4.99.csv'
    ]
    color_array = [
        'r',
        'b',
        'g',
        'y',
        'black',
        'w'
    ]
    radius_array = []

    for file_path in list:
        with open(file_path, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            r = float(0)
            count = 0
            for row in reader:
                r += float(row['r'])
                count += 1

            radius_array.append(r / count)

    ax = plt.gca()
    ax.cla()
    for i, radius in enumerate(radius_array):
        cir = plt.Circle(((x_size + x_shift) / 2, y_size *
                         y_delta / 2), radius, color=color_array[i], fill=False)
        ax.add_patch(cir)

    ax.imshow(peak_plot, extent=[-1, x_size + x_shift, y_size - 0.5, -1])

    plt.legend(['t=0.01', 't=1.0', 't=2.0',
               't=3.0', 't=3.88', 't=4.99'], loc=2)

    plt.savefig("result_plot.png")
    return


# image_function()
# main()
# point_plot()
#image_rad_and_peaks()
