import os
import random
import matplotlib.pyplot as plt
import numpy as np
import math
from mpl_toolkits.axes_grid1 import make_axes_locatable

path = os.getcwd() + "/output"

if not os.path.isdir(path):
        os.mkdir(path)

def get_z(x, y):
    return 1 + math.sin(0.003 * (x - 50) * (y - 50))

def generate_sample(max_x, max_y):
    sample = np.zeros((max_x, max_y))            
    for j in range(max_y):
        for i in range(max_x):
            new_z = int(get_z(i, j) * 10)
            sample[i][j] = new_z
    return sample

def get_s(field):

    fieldSum = field.sum()
    field = field * (1 / fieldSum)

    result = 1
    for j in field:
        for i in j:
            result *= i ** i 
   
    return -math.log(result)

def create_plane(sample, field, SArray, xSArray):
    font_size = 16
    SampleSum = sample.sum()
    #sample = sample * (1 / SampleSum)
    
    fieldSum = field.sum()
    #field = field * (1 / fieldSum)
    
    fig, ax_1 = plt.subplots(figsize=(8, 8))
    img = ax_1.imshow(field, interpolation='nearest')
    ax_1.set_xlabel('x', fontsize=font_size)
    ax_1.set_ylabel('y', fontsize=font_size)
    ax_1.tick_params(labelsize=font_size)
    divider = make_axes_locatable(ax_1)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    plt.colorbar(img, cax=cax)

    # ax_2 = fig.add_subplot(1, 2, 2)
    # ax_2.plot(xSArray, SArray, linewidth = 2, color = (0, 0, 0))
    # ax_2.set_xlabel('N', fontsize=font_size)
    # ax_2.set_ylabel('Entropy', fontsize=font_size)
    
    plt.savefig(path +"/exp_7.png")

def main():
    max_x = 100
    max_y = 100
    step = 1000000
    sample = generate_sample(max_x, max_y)
    sample_sum = sample.sum()

    current_position = np.array([random.randint(0, max_x - 1), random.randint(0, max_y - 1)])
        
    field = np.zeros((max_x, max_y))
    field[current_position[0], current_position[1]] += 1
    field_sum = field.sum()

    x_s_array = []
    s_array = []

    shift_list = np.array(
            [[0, 1], [1, 1], [1, 0], [1, -1],
             [0, -1], [-1, -1], [-1, 0], [-1, 1]])
    shift_list_index = [i for i in range(8)]

    for i in range(step):
        random.shuffle(shift_list_index)
        current_h = float("inf")
        new_position = current_position

        for random_shift in shift_list_index:
            temp_position = current_position + shift_list[random_shift]
            
            expression = temp_position[0] < 0 \
                or temp_position[0] >= max_x \
                or temp_position[1] < 0 \
                or temp_position[1] >= max_y
            if expression:
                continue

            h_1 = sample[temp_position[0], temp_position[1]] - sample_sum / (field_sum + 1) * (field[temp_position[0], temp_position[1]] + 1)
            if h_1 < 0 :
                h_1 = 0
            h_2 = sample[temp_position[0], temp_position[1]] - sample_sum / field_sum * (field[temp_position[0], temp_position[1]])
            if h_2 < 0 :
                h_2 = 0
            h = h_1 - h_2
            if h < current_h:
                current_h = h
                new_position = temp_position

        current_position = new_position
        field[current_position[0], current_position[1]] += 1
        field_sum += 1
        if i % 500 == 0:
            s_array.append(get_s(field.copy()))
            x_s_array.append(i)

    create_plane(sample, field, np.array(s_array), np.array(x_s_array))

main()