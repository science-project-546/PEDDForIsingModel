import os
import matplotlib.pyplot as plt
import numpy as np
import math

path = os.getcwd() + "/output"

if not os.path.isdir(path):
        os.mkdir(path)

n = 20

x_array = np.arange(-0.5 * n, 0.5 * n + 1, 1)
y_array = []
for i in range(n+1):
    y_array.append(1/(2**n) * math.comb(n, i))
y_array = np.array(y_array)

plt.figure(figsize=(12, 6));
font_size = 16
line_width = 8
size_point = 300
plt.xlabel('M', size=font_size)
plt.ylabel(r'$\rho$', size=font_size)
plt.xticks(size=font_size)
plt.yticks(size=font_size)
line_width = 5
plt.plot(x_array,y_array, linewidth=line_width)

index = np.where(x_array == 5.0)

plt.savefig(path +"/exp_9.svg")
