import os
import matplotlib.pyplot as plt
import numpy as np
import math

path = os.getcwd() + "/output"

def P_t(x, t):
    if x <= 0:
        return 1
    return math.exp(-x / (1 * t))

if not os.path.isdir(path):
        os.mkdir(path)

x_array = np.arange(-0.2, 1, 0.01)
y_1_array = np.copy(x_array)
y_2_array = np.copy(x_array)
y_3_array = np.copy(x_array)
y_4_array = np.copy(x_array)

for i, x in enumerate(x_array):
    y_1_array[i] = P_t(x, 0.01)
    y_2_array[i] = P_t(x, 0.05)
    y_3_array[i] = P_t(x, 1)
    y_4_array[i] = P_t(x, 5)

plt.figure(figsize=(12, 6));
font_size = 16
plt.xlabel('ΔE', size=font_size)
plt.ylabel('P', size=font_size)
plt.xticks(size=font_size)
plt.yticks(size=font_size)
line_width = 5
plt.plot(x_array,y_1_array, linewidth=line_width)
plt.plot(x_array,y_2_array, linewidth=line_width)
plt.plot(x_array,y_3_array, linewidth=line_width)
plt.plot(x_array,y_4_array, linewidth=line_width)
#plt.title("Расчёт шанса поворта спина при разны температурах")
plt.legend(['$T = 0.01$', '$T = 0.05$', '$T = 1$', '$T = 5$'], loc=3, prop={'size': font_size})
plt.savefig(path +"/exp_2.png")
