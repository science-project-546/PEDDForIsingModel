import os
import random
import matplotlib.pyplot as plt
import numpy as np
import math
from scipy.interpolate import make_interp_spline

path = os.getcwd() + "/output"

if not os.path.isdir(path):
        os.mkdir(path)

plt.figure(figsize=(16, 8))
font_size = 20
plt.xlabel('X', size=font_size)
plt.ylabel('Y', size=font_size)
plt.xticks(size=font_size)
plt.yticks(size=font_size)
#plt.title("Расчёт шанса поворта спина при разны температурах")
#plt.legend(['$T = 0.01$', '$T = 0.05$', '$T = 1$', '$T = 5$'], loc=3, prop={'size': font_size})

x_array = np.arange(0, 20, 0.01)
y_array = np.sin(x_array)
z_array = np.ones(x_array.size) + 0.5

start_index = 1000
delta_index = 100
delta_index_2 = 350 + delta_index
delta_index_3 = 280 + delta_index_2
mk_vector = np.array([x_array[start_index + delta_index]-x_array[start_index], y_array[start_index + delta_index] - y_array[start_index]])
mk_position = np.array([x_array[start_index], y_array[start_index]])

line_width = 8
size_point = 300
plt.plot(x_array, y_array, linewidth=line_width)
ax = plt.gca()
ax.set_ylim(ymax=3)

plt.plot(x_array[start_index:start_index+delta_index:], y_array[start_index:start_index+delta_index:], linewidth=line_width, zorder=2,color='r')

plt.plot(x_array[start_index+delta_index:start_index+delta_index_2:], z_array[start_index+delta_index:start_index+delta_index_2:], linewidth=line_width, zorder=2,color='b')

plt.plot(x_array[start_index+delta_index_2:start_index+delta_index_3:], y_array[start_index+delta_index_2:start_index+delta_index_3:], linewidth=line_width, zorder=2,color='r')

plt.scatter(x_array[start_index], y_array[start_index], zorder=3, color='black', s=size_point, marker='X')
plt.scatter(x_array[start_index+delta_index], y_array[start_index+delta_index], zorder=3, color='black', s=size_point, marker='>')

plt.scatter(x_array[start_index+delta_index], z_array[start_index+delta_index], zorder=3, color='black', s=size_point, marker='o')
plt.scatter(x_array[start_index+delta_index_2], z_array[start_index+delta_index_2], zorder=3, color='black', s=size_point, marker='>')

plt.scatter(x_array[start_index+delta_index_2], y_array[start_index+delta_index_2], zorder=3, color='black', s=size_point, marker='o')
plt.scatter(x_array[start_index+delta_index_3], y_array[start_index+delta_index_3], zorder=3, color='black', s=size_point, marker='X')

plt.text(x_array[start_index+delta_index] + 0.3 * (x_array[start_index+delta_index_2] - x_array[start_index+delta_index]), z_array[start_index+delta_index_2] - 0.2, "PEDD", size=font_size, color='blue')
plt.text(x_array[start_index] - 3.5, y_array[start_index] + 0.7 * (y_array[start_index+delta_index] - y_array[start_index]), "Монте-Карло", size=font_size, color='r')
plt.text(x_array[start_index+delta_index_2] - 1.5, y_array[start_index] + 0.7 * (y_array[start_index+delta_index] - y_array[start_index]), "Монте-Карло", size=font_size, color='r')

plt.legend(['График функции', 'Путь пройденный методом Монте-Карло', 'Путь пройденный методом PEDD'], loc=2, prop={'size': font_size})

plt.savefig(path +"/exp_8.png")
