import csv
import matplotlib.pyplot as plt
import numpy as np
from enum import Enum
import os.path

path = os.getcwd() + "/output"
file_path = "input/eDeltaFile.csv"

class TypeExperiment(Enum):
    Annealing = 0
    AnnealingPEDD = 1

def get_color(i):
    if TypeExperiment(i) == TypeExperiment.Annealing: return 'g'
    if TypeExperiment(i) == TypeExperiment.AnnealingPEDD: return 'b'
    return 'k'

def main():
    rows = []
    rows.append([])
    rows.append([])
    rows.append([])

        
    with open(file_path, "r") as csvfile:
        plots = csv.reader(csvfile, delimiter=',')
        for row in plots:
            for i, val in enumerate(row):
                rows[i].append(np.float_(val))

    plt.close()
    font_size = 12
    fig = plt.figure(figsize=(12,5))
    ax_1 = fig.add_subplot(1, 2, 1)
    ax_1.set_xlabel('Номер модели', size = font_size)
    ax_1.set_ylabel("$E_{min}$", size = font_size)
    ax_1.tick_params(labelsize=font_size)
    #ax_1.set_title("Разность наименьших энергий", size = font_size);
    
    ax_2 = fig.add_subplot(1, 2, 2)
    ax_2.set_xlabel('Номер модели', size = font_size)
    ax_2.set_ylabel(r"$\bar{E}$", size = font_size)
    ax_2.tick_params(labelsize=font_size)
    #ax_2.set_title("Разность минимальной усредненной энергии", size = font_size)   

    size_point = 64

    for i, row in enumerate(rows):
        if i == 0:
            continue
        if i % 2 != 0:
            ax_1.scatter(rows[0], rows[i], color = get_color(1), marker = 'o', label="deltaMinE_" + TypeExperiment(1).name, s=size_point)

    for i, row in enumerate(rows):
        if i == 0:
            continue
        if i % 2 == 0:
            ax_2.scatter(rows[0], rows[i], color = get_color(0), marker = 's', label="<deltaMinE>_" + TypeExperiment(1).name,  s=size_point)

    plt.savefig(path + "/exp_5.png")

main()