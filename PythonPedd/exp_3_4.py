import os
import random
import time
import matplotlib.pyplot as plt
import numpy as np
import math
from scipy.integrate import simpson

path = os.getcwd() + "/output"

# Кол-во спинов
MaxN = 20
# Сколько раз итерируем
Step = 10000000


def Cnk(X):
    Spin0 = (int)(MaxN / 2 - X)
    Spin1 = MaxN - Spin0
    return math.factorial(Spin0 + Spin1) / (math.factorial(Spin0) * math.factorial(Spin1))

def GenerateSample():
    Sample = np.zeros(MaxN + 1)
    10 * np.ones(MaxN + 1)
    for i in range(MaxN + 1):
        Sample[i] = Cnk(ConvertIndexToM(i))
    return Sample

def GenerateField():
    Sample = np.zeros(MaxN + 1)
    return Sample

def GetRandomIndex():
    return random.randint(0, MaxN - 1)

def GetM(ConfigSpin):
    return 0.5 * ConfigSpin.sum()

def ConvertMToIndex(M):
    return (int)(M + MaxN / 2)

def ConvertIndexToM(Index):
    return (int)(Index - MaxN / 2)

def ConvertSpinToIndex(ConfigSpin):
    def Convert2Binary(x): return "1" if x == 1 else "0"
    String = np.array2string(ConfigSpin, formatter={
                             'float_kind': Convert2Binary}, separator='')
    String = String.replace('[', '')
    String = String.replace(']', '')
    return int(String, 2)

def save_unique_statistic(x_array, y_array):
    y_array = np.array(y_array) * 1.0 
    y_array /= 2**MaxN

    plt.close()
    font_size = 16
    plt.figure(figsize=(8, 6));
    plt.xlabel('Количество итераций', size = font_size)
    plt.ylabel('Доля от всех', size = font_size)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.gca().ticklabel_format(useMathText=True)
    plt.xticks(size=font_size)
    plt.yticks(size=font_size)
    plt.plot(x_array, y_array, linewidth=6)
    plt.hlines(1, x_array[0], x_array[len(x_array) - 1], linestyles='dashed', linewidth=6)
    plt.savefig(path +"/exp_3.png")
    return

def save_distribution(Sample, Field):
    plt.close()
    fig = plt.figure(figsize=(14,7));    
    #fig.text(0.5, 0.05, 'M', ha='center', va='center')
    #fig.text(0.1, 0.5, 'N', ha='center', va='center', rotation='vertical')

    ax_1 = fig.add_subplot(1, 1, 1);
    font_size = 16
    ax_1.tick_params(labelsize=font_size)
    ax_1.set_xlabel('M', size = font_size)
    ax_1.set_ylabel('Ро', size = font_size)
    #ax_1.set_title("Равновесное нормированное распределение", size = font_size);
    
    #ax_2 = fig.add_subplot(1, 2, 2);
    #ax_2.tick_params(labelsize=font_size)
    #ax_2.set_title("Неравновесное распределение", size = font_size);
    #ax_2.set_xlabel('M', size = font_size)
    #ax_2.set_ylabel('N', size = font_size)

    SampleSum = Sample.sum();
    Sample = Sample * float(1 / SampleSum)

    FieldSum = Field.sum();
    Field = Field * (1 / FieldSum)
    
    X = np.array(range((int)(-Sample.size / 2), (int)(Sample.size / 2) + 1, 1));    

    ax_1.plot(X, Field, linewidth=8)
    ax_1.scatter(X, Sample, c="g", s=300, zorder=3)
    plt.legend(['Эталонное распределение', 'Изменяемое распределение'], loc=3, prop={'size': font_size})
    ax_1.set_ylim(ymin=-0.05)

    plt.savefig(path +"/exp_4.png")

    return 

def main():
    startTime = time.time()
    if not os.path.isdir(path):
        os.mkdir(path)

    # Семпл как и Филд есть словарь где ключ-значение представлены
    # как M намагниченость - ключ, а значение - сколько раз алгоритм
    # получал такую намагниченость
    Sample = GenerateSample()
    SampleSum = Sample.sum()

    ConfigSpin = np.ones(MaxN)
    SpinIndex = GetRandomIndex()
    ConfigSpin[SpinIndex] *= -1

    Field = GenerateField()
    FieldIndex = ConvertMToIndex(GetM(ConfigSpin))
    Field[FieldIndex] += 1
    FieldSum = Field.sum()

    ConfigSpinCounter = np.zeros(np.power(2, MaxN))
    ConfigSpinCounter[ConvertSpinToIndex(ConfigSpin)] += 1

    Configs = [i for i in range(MaxN)]

    x_array = []
    y_array = []

    for i in range(Step):
        M = GetM(ConfigSpin)

        NewConfigs = Configs.copy()
        random.shuffle(NewConfigs)

        CurrentH = float("inf")
        NewConfigSpin = ConfigSpin.copy()

        for Config in NewConfigs:
            TempConfigSpin = ConfigSpin.copy()
            TempConfigSpin[Config] *= -1
            FieldIndex = ConvertMToIndex(GetM(TempConfigSpin))

            H1 = Sample[FieldIndex] - SampleSum / \
                (FieldSum + 1) * (Field[FieldIndex] + 1)
            if H1 < 0:
                H1 = 0

            H2 = Sample[FieldIndex] - SampleSum / \
                FieldSum * (Field[FieldIndex])
            if H2 < 0:
                H2 = 0

            H = H1 - H2

            if H < CurrentH:
                CurrentH = H
                NewConfigSpin = TempConfigSpin.copy()

        ConfigSpin = NewConfigSpin
        FieldIndex = ConvertMToIndex(GetM(ConfigSpin))
        Field[FieldIndex] += 1
        FieldSum += 1

        spin_index = ConvertSpinToIndex(ConfigSpin)
        ConfigSpinCounter[spin_index] += 1
        if ConfigSpinCounter[spin_index] < 2:
            x_array.append(i)
            last_index = len(y_array) - 1
            if last_index == -1:
                y_array.append(2)                
            else:
                y_array.append(y_array[last_index] + 1)

    # Времени потрачено
    time_spent = time.time() - startTime
    print("timeSpent: " + str(time_spent))

    save_unique_statistic(x_array, y_array)
    save_distribution(Sample, Field)   
    
    return


main()
