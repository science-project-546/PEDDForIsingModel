import collections
import csv
import os
import random
import shutil
import time
import matplotlib.pyplot as plt
import numpy as np
import math
from multiprocessing import Manager, Process, Queue

path = os.getcwd() + "/output"

a = 1
b = 6

x_shift = 0.5
x_delta = 1
y_delta = 0.5 * math.sqrt(3)

def create_field(x_size, y_size):
    result = []

    for y in range(y_size):
        for x in range(x_size):
            point_x = x * x_delta
            if y % 2 != 0:
                point_x += x_shift
            point_y = y * y_delta
            result.append(np.array([point_x, point_y]))

    return np.array(result)

def h(point: np.array, point_p: np.array):
    return -a * math.exp(-b*((point[0] - point_p[0])**2 + (point[1]-point_p[1])**2))

def get_E(point: np.array, array: np.array):
    result = 0
    for point_p in array:
        result += h(point, point_p)

    return result

def mk(point: np.array, field: np.array, t):
    new_point = np.copy(point)
    phi = random.uniform(0, 2 * math.pi)
    r = 0.01
    new_point += np.array([r * math.cos(phi), r * math.sin(phi)])
    delta = get_E(new_point, field) - get_E(point, field)

    if delta < 0:
        return new_point

    w = math.exp(-delta / (1 * t))
    beta = random.random()
    if beta <= w:
        return new_point

    return point

def image_rad_and_peaks(result, x_size, y_size):

    field = create_field(x_size, y_size)

    peak_plot = []

    for y in np.arange(-1, y_size - 0.5, 0.01):
        tmp = []
        for x in np.arange(-1, x_size + x_shift, 0.01):
            tmp.append(get_E(np.array([x, y]), field))
        peak_plot.append(tmp)

    peak_plot = np.matrix(peak_plot)

    color_array = [
        'r',
        'b',
        'g',
        'y',
        'black',
        'grey'
    ]
    radius_array = []

    for key in result:
        radius_array.append(result[key])

    fig, ax = plt.subplots(figsize=(8, 8))
    #ax = plt.gca()
    ax.cla()
    for i, radius in enumerate(radius_array):
        cir = plt.Circle(((x_size + x_shift) / 2, y_size *
                         y_delta / 2), radius, color=color_array[i], fill=False, linewidth=4)
        ax.add_patch(cir)

    ax.imshow(peak_plot, extent=[-1, x_size + x_shift, y_size - 0.5, -1], cmap='Blues_r', interpolation='nearest')

    t_array_legend = []

    for key in result:
        t_array_legend.append("$T=" + str(key) + "$")

    font_size = 16
    plt.legend(t_array_legend, loc=2, prop={'size': font_size})
    plt.xticks(size=font_size)
    plt.yticks(size=font_size)
    plt.xlabel('X', size=font_size)
    plt.ylabel('Y', size=font_size)

    plt.savefig(path +"/exp_1.png")
    return

def experiment(field, start_point, n, t, result):
    r = 0
    point = np.copy(start_point)

    for i in range(n):
        point = mk(point, field, t)
        new_r = np.linalg.norm(point-start_point)
        r += new_r

    result[t] += float(r) / n
    return



def main():
    startTime = time.time()
    if not os.path.isdir(path):
        os.mkdir(path)
    

    t_array = [0.01, 1, 2, 3, 4, 5]
    count_t_exp = 100
    x_size = 6
    y_size = 6
    n = 100000
    field = create_field(x_size, y_size)
    start_point = np.array([(x_size + x_shift) / 2, y_size * y_delta / 2])

    manager = Manager()
    result = manager.dict()
    for t in t_array:
        result[t] = 0

    jobs = []
    for t in t_array:
        for i in range(count_t_exp):
            p = Process(target=experiment, args=(np.copy(field), np.copy(start_point), n, t, result))
            jobs.append(p)

    job_length = len(jobs)
    for i, job in enumerate(jobs):
        job.start()
        if i % 16 == 0 and i != 0:
            job.join()
            print("Finish: " + str(i) + "/" + str(job_length))

    for job in jobs:
        job.join()

    for t in t_array:
        result[t] /= count_t_exp

    image_rad_and_peaks(result, x_size, y_size)
    # Времени потрачено
    time_spent = time.time() - startTime
    print("timeSpent: " + str(time_spent))
    return

main()