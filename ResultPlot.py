import csv
import matplotlib.pyplot as plt
import numpy as np
from enum import Enum
import os.path

class TypeExperiment(Enum):
    Annealing = 0
    AnnealingPEDD = 1

def getColor(i):
    if TypeExperiment(i) == TypeExperiment.Annealing: return 'g'
    if TypeExperiment(i) == TypeExperiment.AnnealingPEDD: return 'b'
    return 'k'


path = "build/bin/Result/eDeltaFile.csv"
folderPath = "build/bin/Result" 
if os.path.isfile(path) == False :
    path = "Result/eDeltaFile.csv"
    folderPath = "Result" 

rows = []
rows.append([])
rows.append([])
rows.append([])

    
with open(path, "r") as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    for row in plots:
        for i, val in enumerate(row):
            rows[i].append(np.float_(val))

plt.figure().clear()
for i, row in enumerate(rows):
    if i == 0:
        continue
    if i % 2 != 0:
        plt.scatter(rows[0], rows[i], color = getColor(1), marker = 'o', label="deltaMinE_" + TypeExperiment(1).name)
        

plt.xlabel('Experiment')
plt.ylabel('Energy(J)')
plt.title("minE")
plt.legend()
plt.savefig(folderPath + "/minE.svg")

plt.figure().clear()
for i, row in enumerate(rows):
    if i == 0:
        continue
    if i % 2 == 0:
        plt.scatter(rows[0], rows[i], color = getColor(0), marker = 's', label="<deltaMinE>_" + TypeExperiment(1).name)

plt.xlabel('Experiment')
plt.ylabel('Energy(J)')
plt.title("<minE>")
plt.legend()
plt.savefig(folderPath + "/<minE>.svg")
plt.figure().clear()