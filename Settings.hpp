#pragma once
#include "Definition.hpp"

namespace peddIM {

enum class TypeExperiment {
    //Не менять порядок!
    Annealing,
    AnnealingPEDD,
    Count
};

std::string typeExperiment2String(TypeExperiment experiment) {
    switch (experiment) {
    case TypeExperiment::Annealing:
        return "Annealing";
    case TypeExperiment::AnnealingPEDD:
        return "AnnealingPEDD";
    case TypeExperiment::Count:
        return "Count";
    }

    return "Unknown";

}

struct Settings {

    //Experiment
    int64_t countExperiment;
    int64_t countIteration;
    double kBoltzmann;
    double startT;
    double startAlpha;

    //Annealing
    int64_t countPlateau;
    int64_t dropTStep;
    double factorT;
    double smallW; // Константа определяющая, когда шанс переворота спина в МК маленький

    //AnnealingPedd
    int64_t timeMK;
    int64_t timePedd;
    double smallWAnnealingPedd;

    //Debug
#ifdef DEBUGMODE
    int64_t statisticsStep;
#endif

    //Model
    int64_t sizeModel;
    std::vector<double> indexToLength;
    std::vector<int64_t> sample;
    int64_t sampleSum;

    template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & countExperiment;        
        ar & countIteration;
        ar & kBoltzmann;
        ar & startT;
        ar & startAlpha;

        ar & countPlateau;
        ar & dropTStep;
        ar & factorT;
        ar & smallW;

        ar & timeMK;
        ar & timePedd;
        ar & smallWAnnealingPedd;

#ifdef DEBUGMODE
        ar & statisticsStep;
#endif

        ar & sizeModel;
        ar & indexToLength;
        ar & sample;
        ar & sampleSum;
    }
};

struct ModelSettings {

    //Model
    std::vector<double> interactions;
    double h;
    double meanT;


    template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & interactions;
        ar & h;
        ar & meanT;
    }
};

}
