#pragma once

#include "Definition.hpp"
#include "Settings.hpp"
#include "FreeFunctions.hpp"

namespace peddIM {

struct InputHandler {
public:

    std::string inputFolder;
    std::string settingsFileName;
    std::vector<std::string> settingRows;

    std::string modelFileName;
    std::vector<std::string> modelCols;
    int countJ;


private:
    InputHandler() {
        inputFolder = "Input";

        settingsFileName = "Settings.csv";
        settingRows = {
            //Experiment
            "countExperiment",
            "countIteration",
            "kBoltzmann",
            "startT",
            "startAlpha",
            //Annealing
            "countPlateau", // Отсчет данный системе на сваливание в локальный минимум
            "dropTStep",
            "factorT",
            "smallW",           
            //AnnealingPEDD
            "timeMK",
            "timePedd",
            "smallWAnnealingPedd",
#ifdef DEBUGMODE
            //Debug
            "statisticsStep",
#endif
            //Model
            "sizeModel",
            "indexToLength",
        };

        modelFileName = "Model.csv";
        countJ = 2;
        modelCols.push_back("N");
        for(auto i = 0; i < 2; i++) {
           modelCols.push_back("J" + std::to_string(i + 1));
        }
        modelCols.push_back("H");
        modelCols.push_back("meanT");
    }
    ~InputHandler() { }

    InputHandler(const InputHandler& other) = delete;
    InputHandler& operator=(const InputHandler& other) = delete;
    InputHandler(InputHandler&& other) = delete;
    InputHandler& operator=(InputHandler&& other) = delete;

public:

    static InputHandler* instance() {
        static InputHandler instant;
        return &instant;
    }

    Settings getSettings() {

        rapidcsv::Document file(inputFolder + "/" + settingsFileName, rapidcsv::LabelParams(-1, 0));
        Settings settings;

        auto index = 0;
        settings.countExperiment = file.GetRow<int>(settingRows[index++])[0];
        settings.countIteration = file.GetRow<int>(settingRows[index++])[0];
        settings.kBoltzmann = file.GetRow<double>(settingRows[index++])[0];
        settings.startT = file.GetRow<double>(settingRows[index++])[0];
        settings.startAlpha = file.GetRow<double>(settingRows[index++])[0];

        settings.countPlateau = file.GetRow<double>(settingRows[index++])[0];
        settings.dropTStep = file.GetRow<double>(settingRows[index++])[0] * settings.countIteration;
        settings.factorT = file.GetRow<double>(settingRows[index++])[0];
        settings.smallW = file.GetRow<double>(settingRows[index++])[0];

        settings.timeMK = file.GetRow<int>(settingRows[index++])[0];
        settings.timePedd = file.GetRow<int>(settingRows[index++])[0];
        settings.smallWAnnealingPedd = file.GetRow<double>(settingRows[index++])[0];

#ifdef DEBUGMODE
        settings.statisticsStep = file.GetRow<double>(settingRows[index++])[0] * settings.countIteration;
#endif

        settings.sizeModel = file.GetRow<int>(settingRows[index++])[0];
        settings.indexToLength = file.GetRow<double>(settingRows[index++]);
        std::sort(settings.indexToLength.begin(), settings.indexToLength.end());
        settings.sample = getSample(settings);
        settings.sampleSum = std::accumulate(settings.sample.cbegin(), settings.sample.cend(), (int64_t)0);
        /*
        std::cout << settings.countExperiment << "\n";
        std::cout << settings.countIteration << "\n";
        std::cout << settings.kBoltzmann << "\n";
        std::cout << settings.startT << "\n";
        std::cout << settings.startAlpha << "\n";
        std::cout << settings.deltaEMeanE << "\n";
        std::cout << settings.countPlateau << "\n";
        std::cout << settings.dropTStep << "\n";
        std::cout << settings.factorT << "\n";

        std::cout << settings.smallW << "\n";

        std::cout << settings.dropAlphaStep << "\n";
        std::cout << settings.factorAlpha << "\n";

#ifdef DEBUGMODE
        std::cout << settings.statisticsStep << "\n";
#endif

        std::cout << settings.sizeModel << "\n";
        for(auto e : settings.indexToLength)
            std::cout << e << " ";
        std::cout << "\n";

        for(auto e : settings.sample)
            std::cout << e << " ";
        std::cout << "\n";
        std::cout << settings.sampleSum << "\n";
        */

        return settings;
    }

    void getModelSettings(std::vector<ModelSettings>& settings) {
        auto inputHandler = InputHandler::instance();
        const auto modelFilePath = inputHandler->inputFolder + "/" + inputHandler->modelFileName;
        rapidcsv::Document file(modelFilePath, rapidcsv::LabelParams(0, 0));
        for(auto i = 0; i < file.GetRowCount(); i++) {
            ModelSettings modelSettings;

            std::vector<double> model = file.GetRow<double>(std::to_string(i));
            for(auto j = 0; j < model.size() - 2; j++) {
                modelSettings.interactions.push_back(model[j]);
            }
            modelSettings.h = model[model.size() - 2];
            modelSettings.meanT = model[model.size() - 1];
            settings.push_back(modelSettings);

            /*
            for(auto e : modelSettings.interactions)
                std::cout << e << " ";
            std::cout << "\n";
            std::cout << modelSettings.h << "\n";
            std::cout << modelSettings.meanT << "\n";
            */
        }


    }

private:

    static std::vector<int64_t> getSample(const Settings &settings) {

        auto maxLengthSpin = std::fabs(settings.indexToLength[0]);
        auto mSize = static_cast<int64_t>(2 * maxLengthSpin * settings.sizeModel + 1);
        std::vector<int64_t> sample(mSize, 8);

        double mMax = maxLengthSpin * settings.sizeModel;
        auto gaussian = [mMax](int64_t x) {
            return std::exp(-log(mMax * mMax) / (mMax * mMax) * x * x);
        };

        for (auto i = 0; i < mSize; i++) {
            if (i == 0 || i == mSize - 1) {
                sample[i] = 1;
                continue;
            }
            auto m = convertIndexToM(i, settings);
            auto alpha = gaussian(m);
            sample[i] = static_cast<int64_t>(alpha * settings.countIteration * 0.1);
            if (sample[i] < 10) sample[i] = 10;
        }

        return sample;
    }

};
}
