import csv
import matplotlib.pyplot as plt
import numpy as np
from enum import Enum
import os.path

class TypeExperiment(Enum):
    Annealing = 0
    AnnealingPEDD = 1

tPlotFileNames = []
alphaPlotFileNames = []
fieldSampleFileNames = []
annealingCountNames = []
ePlotFileNames = []
TypeExperimentCount = 2 

for type in TypeExperiment:
    tPlotFileNames.append(str(3) + "_TPlotFile_" + type.name + ".csv")
    alphaPlotFileNames.append(str(4) + "_AlphaPlotFile_" + type.name + ".csv")
    fieldSampleFileNames.append(str(5) + "_FieldSampleFile_" + type.name + ".csv")
    annealingCountNames.append(str(6) + "_AnnealingCount_" + type.name + ".csv")
    ePlotFileNames.append(str(7) + "_ePlotFile_" + type.name + ".csv")

#print(tPlotFileNames)
#print(alphaPlotFileNames)
#print(fieldSampleFileNames)
#print(annealingCountNames)
#print(ePlotFileNames)

def ePlot(folder):
    for fileName in ePlotFileNames:
        rows = []

        fpath = str(folder) + "/" + fileName
        if os.path.isfile(fpath) == False:
            continue

        plt.figure().clear()
        with open(fpath, "r") as csvfile:
            plots = csv.reader(csvfile, delimiter=',')
            for row in plots:
                rows.append(np.float_(row))

        for i, row in enumerate(rows):
            if i == 0:
                continue
            plt.plot(rows[0], rows[i], label="Energy_" + str(i))

        plt.xlabel('Step')
        plt.ylabel('Energy(J)')
        plt.title("Energy spin model")
        plt.legend()
        plt.savefig(fpath + ".svg")

def fieldSamplePlot(folder):
    for fileName in fieldSampleFileNames:
        rows = []

        fpath = str(folder) + "/" + fileName
        if os.path.isfile(fpath) == False:
            continue

        plt.figure().clear()
        with open(fpath, "r") as csvfile:
            plots = csv.reader(csvfile, delimiter=',')
            for row in plots:
                rows.append(np.float_(row))

        for i, row in enumerate(rows):
            if i == 0:
                continue
            if i == 1:
                plt.plot(rows[0], rows[i], label="sample")
                continue

            plt.plot(rows[0], rows[i], label="field_" + str(i + 1))

        plt.xlabel('Magnetization')
        plt.ylabel('Count')
        plt.title("Sample and field")
        plt.legend()
        plt.savefig(fpath + ".svg")

def tPlot(folder):
    for fileName in tPlotFileNames:
        rows = []

        fpath = str(folder) + "/" + fileName
        if os.path.isfile(fpath) == False:
            continue

        plt.figure().clear()
        #plt.figure(figsize=(40, 5))

        with open(fpath, "r") as csvfile:
            plots = csv.reader(csvfile, delimiter=',')
            for row in plots:
                rows.append(np.float_(row))

        for i, row in enumerate(rows):
            if i == 0:
                continue
            plt.plot(rows[0], rows[i], label="t_" + str(i))

        plt.xlabel('Step')
        plt.ylabel('T(J)')
        plt.title("T spin model")
        plt.legend()
        plt.savefig(fpath + ".svg")
 
def alphaPlot(folder):
    for fileName in alphaPlotFileNames:
        rows = []

        fpath = folder + "/" + fileName
        if os.path.isfile(fpath) == False:
            continue

        plt.figure().clear()
        with open(fpath, "r") as csvfile:
            plots = csv.reader(csvfile, delimiter=',')
            for row in plots:
                rows.append(np.float_(row))

        for i, row in enumerate(rows):
            if i == 0:
                continue
            plt.plot(rows[0], rows[i], label="alpha_" + str(i))

        plt.xlabel('Step')
        plt.ylabel('Alpha')
        plt.title("Alpha PEDD")
        plt.legend()
        plt.savefig(folder + "/" + fileName + ".svg")
 
path = "build/bin/Debug/"
if os.path.isdir(path) == False :
    path = "Debug"

subfolders = [ f.path for f in os.scandir(path) if f.is_dir() ]
for folder in subfolders:
    tPlot(folder)
    alphaPlot(folder)
    fieldSamplePlot(folder)
    ePlot(folder)
