#include <iostream>
#include "../PEDDForIsingModel.hpp"
#include "../Definition.hpp"

int main(int argc, char* argv[]) {
    using namespace peddIM;

    auto inputHandler = InputHandler::instance();

    if (!fs::is_directory(inputHandler->inputFolder)) boost::filesystem::create_directories(inputHandler->inputFolder);

    const auto modelFilePath = inputHandler->inputFolder + "/" + inputHandler->modelFileName;

    {
        std::ofstream f(modelFilePath);

        for (auto i = 0; i < inputHandler->modelCols.size(); i++) {

            f << inputHandler->modelCols[i];
            auto symbol = (i != inputHandler->modelCols.size() - 1) ? "," : "\n";
            f << symbol;
        }
        f.close();
    }

    boost::random_device rd("/dev/urandom");
    RNGType generator(rd());

    boost::uniform_real<> getRandJ(0.01f, 1.f);
    boost::uniform_real<> getRandH(0, 0.5f);
    boost::uniform_01<> getRand01;

    std::vector<ModelSettings> modelSettingsArray;
    auto count = 100;

    auto settings = inputHandler->getSettings();
    ExperimentHandler expHandler(settings);

    for(auto i = 0; i < count; i++){
        std::vector<double> interactions;
        for(auto j = 0; j < inputHandler->countJ; ++j) {
            interactions.push_back(getRandJ(generator));
        }
        interactions[1] *= -1;
        for(auto j = 2; j < interactions.size(); j++) {
            if(getRand01(generator) > 0.5) {
                interactions[j] *= -1;
            }
        }
        std::shuffle(interactions.begin(), interactions.end(), generator);

        ModelSettings modelSettings;
        modelSettings.interactions = interactions;
        modelSettings.h = 0; //getRandH(generator);
        expHandler.setModelSettings(modelSettings);
        modelSettings.meanT = expHandler.getMeanT();
        modelSettingsArray.push_back(modelSettings);
    }

    std::ofstream modelFile(modelFilePath, std::ofstream::app);
    for(auto i = 0; i < count; i++){

        modelFile << i << ",";
        for(const auto &j : modelSettingsArray[i].interactions){
            modelFile << j << ",";
        }
        modelFile << modelSettingsArray[i].h << ",";
        modelFile << modelSettingsArray[i].meanT << "\n";
    }

    modelFile.close();

    return 0;
}
