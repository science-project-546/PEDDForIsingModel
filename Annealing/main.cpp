#include <iostream>
#include "../PEDDForIsingModel.hpp"

int main(int argc, char* argv[]) {
    using namespace peddIM;
    mpi::environment env(argc, argv);
    mpi::communicator world;

    if(world.size() < 2) {
        std::cout << "Need more rank!" << std::endl;
        return 0;
    }
    if(world.rank() == 0) {
        timer::auto_cpu_timer timer;
        MainRankHandler rank;
        rank.execute();
        timer.stop();
        boost::chrono::duration<double> elapsed = boost::chrono::nanoseconds(timer.elapsed().user);
        std::cout << "\nTime: " << elapsed.count() << " s" << "\n";

        system("python ResultPlot.py");

#ifdef DEBUGMODE
        system("python DebugPlot.py");
#endif
    }
    else {
        SupportRankHandler rank;
        rank.execute();
    }

    return 0;
}
